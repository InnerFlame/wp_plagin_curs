<?php
/*
Plugin Name: Admin User list
Plugin URI: http://www.alexzdesign.ru/blog/adminuserlist/
Description: Вывод контактной информации о зарегистрированных пользователях
Version: 0.1
Author: Alexander A. Zaramenskikh
Author URI: http://www.alexzdesign.ru/
*/

function add_jquery_data() {
    global $parent_file;

   ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
   <?
}

add_filter('admin_head', 'add_jquery_data');


function userlist_init(){
	$icon = '';
	add_object_page('курс валют', 'Курс валют', 1, basename(__FILE__), 'clients_email', $icon);
}

add_action('admin_menu','userlist_init');

function clients_email() {
	global $wpdb;

if(isset($_POST['save']))
{
	//
	$date = mysql_real_escape_string($_POST['date']);
	$usd1 = str_replace(',','.',mysql_real_escape_string($_POST['usd1']));
	$rub1 = str_replace(',','.',mysql_real_escape_string($_POST['rub1']));
	$eur1 = str_replace(',','.',mysql_real_escape_string($_POST['eur1']));
	$usd2 = str_replace(',','.',mysql_real_escape_string($_POST['usd2']));
	$rub2 = str_replace(',','.',mysql_real_escape_string($_POST['rub2']));
	$eur2 = str_replace(',','.',mysql_real_escape_string($_POST['eur2']));
	$usd3 = str_replace(',','.',mysql_real_escape_string($_POST['usd3']));
	$rub3 = str_replace(',','.',mysql_real_escape_string($_POST['rub3']));
	$eur3 = str_replace(',','.',mysql_real_escape_string($_POST['eur3']));

	$wpdb->query("insert into cur(`date`, `usd1`, `rub1`, `eur1`, `usd2`, `rub2`, `eur2`, `usd3`, `rub3`, `eur3`) values('$date', '$usd1', '$rub1', '$eur1', '$usd2', '$rub2', '$eur2', '$usd3', '$rub3','$eur3')
							ON DUPLICATE KEY UPDATE
							`usd1`='$usd1', `rub1`='$rub1', `eur1`='$eur1',`usd2`='$usd2', `rub2`='$rub2', `eur2`='$eur2',`usd3`='$usd3', `rub3`='$rub3', `eur3`='$eur3'
							;
	");

}
	$rows = $wpdb->get_results("select *,DATE_FORMAT(`date`,'%d.%m%.%Y') as dt from cur where `date`<='".date('Y-m-d')."' order by `date` desc limit 100");
	if(isset($_GET['edit'])){
		$edit_cur = $wpdb->get_row("select *,DATE_FORMAT(`date`,'%d.%m%.%Y') as dt from cur where `id`=".intval($_GET['edit'])."");
	}

?>
<br/>
<form method='post'>
Дата
<input name='date' class='datepicker' id='datepicker' 
	<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->date."'";
	}
	?>
	>
	<script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>
<table>
<tr>
	<th>Валюта</th>
	<th>Покупка</th>
	<th>Продажа</th>
	<th>НБУ за 100 одиниць</th>
</tr>

<tr>
	<th>USD</th>
	<th><input name='usd1' 
		<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->usd1."'";
	}
	?>

	/></th>
	<th><input name='usd2'
			<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->usd2."'";
	}
	?>

		/></th>
	<th><input name='usd3' 
				<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->usd3."'";
	}
	else
	{
	?>

			value='<?=$rows[0]->usd3?>'<?}?>/></th>
</tr>
<tr>
	<th>EUR</th>
	<th><input name='eur1'
				<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->eur1."'";
	}
	?>

	/></th>
	<th><input name='eur2'
					<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->eur2."'";
	}
	?>

		/></th>
	<th><input name='eur3' 
						<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->eur3."'";
	}
	else
	{
	?>

			value='<?=$rows[0]->eur3?>'<?}?>/></th>
</tr>
<tr>
	<th>RUB</th>
	<th><input name='rub1' 
				<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->rub1."'";
	}
	?>

	/></th>
	<th><input name='rub2'
					<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->rub2."'";
	}
	?>

		/></th>
	<th><input name='rub3' 
						<?
	if($edit_cur)
	{
		echo "value='".$edit_cur->rub3."'";
	}
	else
	{
	?>

			value='<?=$rows[0]->rub3?>'<?}?>/></th>
</tr><br/><br/>
</table>
<input type="submit" name='save' value="Сохранить"/>
</form>
<br/>
	<table border='1px'>
		<tr>
		<td>Дата</td>
		<td>USD</td>
		<td>EUR</td>
		<td>RUB</td>
		<td>Действия</td>
		</tr>
<?
foreach($rows as $row)
{
	?>
	<tr>
		<td><?=$row->date?></td>
		<td><?=$row->usd1?> / <?=$row->usd2?> / <?=$row->usd3?></td>
		<td><?=$row->eur1?> / <?=$row->eur2?> / <?=$row->eur3?></td>
		<td><?=$row->rub1?> / <?=$row->rub2?> / <?=$row->rub3?></td>
		<td><a href='?page=myplug.php&edit=<?=$row->id?>'>Изменить</a></td>
	</tr>
	<?
}
?>
	</table>	

	<?


/*
//require_once('/wp-includes/wp-db.php');
global $wpdb;
/* пока не добавил ссылку на класс и глобально не объявил — ничего не хотело работать */
//echo "<p><strong>Электронная почта</strong></p>";
/* просто строка с надписью Электронная почта */
//$sql = "SELECT user_email FROM wp_users";
/* SQL-запрос в БД */
//$arr = $wpdb->get_results($sql,"ARRAY_A");
/* получание результата и дальнейший вывод, кстати методы получения могут быть разные, как и аргументы — читаем $wpdb */
//for ($i=0;$i<count($arr);$i++) {
//echo "<p>".$arr[$i]['user_email']."</p>";
//}
}

// Creating the widget 
class wpb_widget extends WP_Widget {

private $wpdb;
function __construct() {
	global $wpdb;
$this->wpdb = &$wpdb;

parent::__construct(
// Base ID of your widget
'wpb_widget', 

// Widget name will appear in UI
__('WPBeginner Widget', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output

	$cur = $this->wpdb->get_row("select *,DATE_FORMAT(`date`,'%d.%m%.%Y') as dt from cur where `date`<='".date('Y-m-d')."' order by `date` desc limit 1");
	$cur_cart = $this->wpdb->get_row("select *,DATE_FORMAT(`date`,'%d.%m%.%Y') as dt from cur_cart where `date`<='".date('Y-m-d')."' order by `date` desc limit 1");
?>
<div class="socials">

<div class="block" style='width:300px;'>
    <div class="container">

        <ul class="tabs">
            <li style="font-size: 12px" class="tab-link current" data-tab="tab-1">Курси валют</li>
            <li style="font-size: 12px" class="tab-link" data-tab="tab-2">Курси валют з БПК</li>

        </ul>

        <div id="tab-1" class="tab-content current">
            <table style='margin-top:-10px; width:220px '>

                <tr>

                    <td></td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>Купiвля</td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>Продаж</td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>НБУ</td>

                </tr>

                <tr>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>USD</td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->usd1?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->usd2?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->usd3?></td>

                </tr>

                <tr>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>EUR</td>

                    <td style="font-size: 12px"><?=$cur->eur1?></td>

                    <td style="font-size: 12px"><?=$cur->eur2?></td>

                    <td style="font-size: 12px"><?=$cur->eur3?></td>

                </tr>

                <tr>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>RUB</td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->rub1?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->rub2?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur->rub3?></td>

                </tr>

            </table>

            <div class="dates">станом на <?=$cur->dt?></div>
        </div>
        <div id="tab-2" class="tab-content">
            <table style='margin-top:-10px; width:220px '>

                <tr>

                    <td></td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>Купiвля</td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>Продаж</td>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>НБУ</td>

                </tr>

                <tr>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>USD</td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur_cart->usd1?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur_cart->usd2?></td>

                    <td style='background-color:#eaf9fd;font-size: 12px'><?=$cur_cart->usd3?></td>

                </tr>

                <tr>

                    <td style='padding-left:5px;padding-right:5px;font-size: 12px'>EUR</td>

                    <td style="font-size: 12px"><?=$cur_cart->eur1?></td>

                    <td style="font-size: 12px"><?=$cur_cart->eur2?></td>

                    <td style="font-size: 12px"><?=$cur_cart->eur3?></td>

            </table>

            <div class="dates">станом на <?=$cur_cart->dt?></div>
        </div>


    </div><!-- container -->

</div>

</div>
<?
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );


?>